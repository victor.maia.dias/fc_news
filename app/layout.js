import "./globals.css";
import { Header } from "@/components/header";
import { AuthProvider } from "@/providers/auth-provider";


export const metadata = {
  title: "FC News",
  description: "Portal de noticias FC Teste",
};


export default function RootLayout({ children }) {
  
  return (
    <AuthProvider>
      <html lang="en">
        <body>
          <Header />
          <div className="flex justify-center items-center">{children}</div>
        </body>
      </html>
    </AuthProvider>
  )
}